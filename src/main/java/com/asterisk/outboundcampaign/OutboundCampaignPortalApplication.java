package com.asterisk.outboundcampaign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OutboundCampaignPortalApplication {

	public static void main(String[] args) {
		SpringApplication.run(OutboundCampaignPortalApplication.class, args);
	}

}
